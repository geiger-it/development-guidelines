# Microsoft Azure - Active Directory

----
![](img/azure-ad-and-local-sign-in-buttons.png)

As of 2022-09-01 kerberos, or any method where a cleartext password passes through the application to 
authenticate against Active Directory, is deprecated and no longer acceptable. Any application dealing with PII 
(Personally Identifiable Information), payments, pricing, or other privileged information must be protected by 
multi-factor authentication. 

"Sign in with Microsoft" takes care of single sign-on, access revocation, and MFA, so that we don't have to worry 
about SMS text message services, QR code generation, storing user OTPs, resets, etc.

## Application Developer Procedure

### Prepare the local development environment

 - Https is required - meaning a self-signed TLS/SSL certificate will need to be issued and port 443 open on your 
   local virtual machine. You will get a warning in your browser about the security of the certificate; you will 
   just need to add it as an exception. Fortunately, the latest Laravel-quickstart project will have this 
   provisioned for you.
 - Install Socialite [https://laravel.com/docs/9.x/socialite](https://laravel.com/docs/9.x/socialite)
 - Install the Azure AD Provider
   Azure AD Provider [https://socialiteproviders.com/Microsoft-Azure/#installation-basic-usage](https://socialiteproviders.com/Microsoft-Azure/#installation-basic-usage)
   
### Request an Azure AD App Registration

 Create a ticket and request the Infrastructure team create a new application registration for Azure AD.

```
Hi Infrastructure team,

I have a new application needing an app registration in Microsoft Azure AD. Here is the following information you'll 
need to fulfill this request:
 - Environment: Staging (qa-geiger.com)
 - Application Name:  PopUp Stores
 - Application Description: A multi-site ecommerce application allowing Sales Partners, Geiger associates, and IT 
 Admins to log into the management backend of the stores via https://groupbuylimited.com/login.
 - Redirect URI:  https://plex.local/auth/azure/inbound
 - Client Secret Duration: 24 months (please provide a new client secret every 18 months from issuance)
 
Please supply me with the following (either through LastPass or an encrypted message):
 - Application (client) ID:
 - Directory (tenant) ID: 
 - The Client Secret ID: 
 - The Client Secret Value: 
 
 Thanks,
 App Dev 
```

The infrastructure team will respond with the following information:

```
Hi App Developer,

 - Application (client) ID: e955179d-7cd0-405d-8ca6-7c0c82838846
 - Directory (tenant) ID: 9ca54e7f-360e-4bf6-9ce3-0c4e6167b8f8
 - The Client Secret ID: 1302103d-b58c-4dd5-9e83-da8efb9265fb
 - The Client Secret Value: EIe8Q~-P.r~uYwi5bFYmBG7UC5_jxGOOf0YlBcfP
 
Remember, do not share or re-use these authentication secrets across applications
or with any 3rd parties, including our business associates.
 
With love and respect,
Your Infra Friend
```

### Storing Secrets
 - **Local:** Your personal Vagrantfile (gitignore'd).
 - **Staging:** Encrypted in `provision/ansible/group_vars/qa/vars.yml` using ansible-vault. 
 - **Production:** Request DevOps to encrypt in `provision/ansible/group_vars/prod/vars.yml`

### Shared Role-Based Accounts

Rather than paying for and maintaining a large base of users (testers and developers) in a staging environment, we 
will maintain a list of role-based accounts that can be shared across testers and developers.

| Name               | Email                               |
|--------------------|-------------------------------------|
| Sales Partner      | sales-partner@qa-geiger.com         |
| Sales Associate    | sales-associate@qa-geiger.com       |
| Business Associate | business-associate@qa-geiger.com	   |
| Business Admin     | business-admin@qa-geiger.com        |
| IT Admin           | it-admin@qa-geiger.com              |

**Passwords:** See "Shared Staging AD Login Accounts" note in LastPass

Remember, staging accounts are insecure and must not contain any production or real-world data - like customer 
information, order data, pricing, client secrets, etc.

Ensure a stricter rate limit is set for the application's Azure auth routes. Consult with an Architect or DevOps for 
your application's specific needs.


## Infrastructure Procedure

Ensure requests for production (geiger.com, crestline.com) app registrations come from an authorized DevOps associate.
All developers in IT are authorized to request staging (qa-geiger.com) app registrations.

1. **Create a new App Registration**

    For better visibility and to prevent mistakes, append name registrations with the environment in parentheses. 
   `(QA)` for staging or `(Prod)` for production. 
   
    E.g. `"PopUp Stores (QA)"`

2. **Supported Account Types**
    Unless otherwise specified, choose "Accounts in this organizational directory only"
   // TODO: make sure this works for both geiger.com and crestline.com

3. **Redirect URI**
    - Validate the requestor's redirect URI. 
        - The URI must use the `https` protocol.
        - For the production environment, verify the domain ownership as belonging to Geiger. Local development URIs 
        - Note that in the staging environment, non-standard TLDs may be used (e.g. `.loc` or `.local`). 
    - Enter the requestor's verified `'Web'` URI(s) in Azure and `Register` the application.

4. **Create a new Client Secret**

    - **Description:** The same as the App name with its environment, appended with the date in `YYYY-MM-DD` format. 
      E.g. `PopUp Stores (QA) 2022-09-01`
    
    Set the expiration to 24 months. Create a reminder to issue a new client secret 18 months from each day of issuance 
   (recurring). Communicate and follow-up with the DevOps team to ensure the new client secret is effective so that 
   the old client secret can be removed.
   