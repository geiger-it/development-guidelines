# PHP Development Guidelines
These guidelines provide requirements for developing custom PHP solutions.

Geiger developers and development partners must agree to and adhere to the [Software Engineering Code of Ethics and Professional Practice](https://www.researchgate.net/publication/278417404_Software_Engineering_Code_of_Ethics_and_Professional_Practice)


** Useful Resources: **

[https://phptherightway.com](https://phptherightway.com)

[https://www.php-fig.org](https://www.php-fig.org)


## Development Environments

### Local Developoment Environment
*Local* development should run in an environment that matches closely to production. It is acceptable to install mysql, 
redis, and other services on the same virtual host in development for convenience. Personal secrets (access credentials)
are stored in the project's Vagrantfile (which is omitted from version control) under the ansible extra vars section.
The variables in this file will be picked up by Ansible and generally get written to the generated .env (environment) file.
Application developers are responsible for creating the Ansible playbook and variables for their local environment.
Use [Vagrant](https://www.vagrantup.com/) and [Virtualbox](https://www.virtualbox.org/wiki/Downloads) to build and manage
your local app server.

### *Staging* / *QA* Environment(s), 
Staging environments should match as close to production as possible. The process for deployment, the set of services,
the configuration variables, and infrastructure should mirror a production-like environment. No shortcuts should be made
(e.g. installing the database or redis on the same application host).
Applications must have a plan to scale horizontally - which means adding multiple application hosts to handle more traffic. 
When users are hopping from one application host to another, we cannot have 2 versions of the database on each host, we 
cannot use local file storage (for cache, sessions, or file uploads) because sessions will be dropped and files inaccessible when 
a user bounces bewteen the two or more load balanced hosts. 
In certain situations, it's acceptable to have multiple staging environments for testing different features at different times
with different sets of users.
Application developers are responsible for creating the Ansible playbook and variables for the staging environment.
Secrets are managed using [ansible-vault](https://docs.ansible.com/ansible/latest/cli/ansible-vault.html). All staging 
environment's encrypted vars use the same password for convenience **based on the requirement that staging environments never 
hold production data or secrets.**


### Pre-production Environment
In the rare case when production data needs to be tested or staged in a non-production environment, a temporary pre-production
environment may be created. Ensure that the environment is deployed and treated as a production environment. Ensure access to 
the system is restricted and that all production services and protections are in place (e.g. Firewalls, AV, agents, etc.). Ensure 
the environment is destroyed as early as possible (schedule reminders).


### Production Environment

Access to production systems is limited to the infrastructure team and DevSecOps. The DevSecOps team will be responsible for 
creating and securing the Ansible playbook and encrypted variables for the production environment.

### All Environments

Server time will always be set in UTC on the server. Be sure to account for localization in the application (whether setting 
user-specific timezones or one global timezone for the app). If you need to account for more than 1 timezone in your 
application, make sure the application is set to UTC by default.

Standard infrastructure for development:

  - Linux: Ubuntu Focal Fossa 20.04 LTS x64
  - Nginx, MySQL, and php-fpm (latest stable versions)
  - PHP ^8.1


## Version Control
We use [git](https://git-scm.com/) and [bitbucket](bitbucket.org) 
 as the central origin for source control. An architect will create repositories and assign ownership to the lead 
 developer. Our repositories must remain private, which requires ssh key pairs on any environment accessing the 
 repository.

[Tower](https://www.git-tower.com) is an optional GUI application for Mac users using git.

Each production release of the main branch will require a tagged version for an IT audit before getting deployed to 
a production system. This tagging system will be used in the event that the application needs to roll back to the 
last working version - which may be any number of commits behind. Tags will follow the 
[Semantic Versioning 2.0.0](http://semver.org/) specification for libraries and packages.

`git tag -a v1.4.2 -m "Hotfix to handle remote service timeout."`

## Database Management
Prevent making **irreversible data integrity errors** by understanding and defining relations, constraints, and 
 referential actions. Deferring the responsibilities of integrity to the application programming makes the data model
 confusing for other developers and leaves an open door to bad data that may not be possible to fix.

> When Simplemerce ran out of 32-bit integers for 'products', mysql intelligently began re-using deleted ids to 
increment on. This would have been ok if all of the relations cascaded with the deletion of the product. Instead, new 
products were not only getting their newly created 'attributes', but also referencing the previous decade-old deleted 
products' attributes as well.

Relational databases must maintain foreign key relationships with explicit 
 [referential actions](https://dev.mysql.com/doc/refman/5.7/en/create-table-foreign-keys.html#idm140475317576864)
 (even if you're declaring "NO ACTION"). This ensures that you have thought about the relationship and how to handle
 change in your data while maintaining integrity of the schema. "CASCADE" is the most common type of action. But be 
 thoughtful and explicit when deciding these.

Database migration tools provide a consistent and testable way to ensure that the schema can be rolled back or 
 brought forward through change management without loss to data or structural integrity.  We require either a plan or 
 tool to be in place for migrations, but any plan or tool can be used so as long as it does not include remembering to
 manually execute ad-hoc queries during deployments.

Passwords, tokens, and other secrets must never be stored in plain text in the repository. If an accidental push is made
to a remote repository containing sensitive information, do not push a subsequent fix to remove the information; contact
the repository owner (and Architect) to rotate out the exposed secret credentials and rewrite history to remove it from 
the remote repository. Consult IT's DevSecOps Team for the latest requirements and recommendations for secure hashing 
algorithms.

Storing data as json in a relational database should be reserved for caching or backups. If the data contains information
 that needs to be accessed as part of a collection (e.g. product configuration containing a price), this data should be
 stored relationally instead.

## Email
Applications in development and qa should only use fake/stubbed data; ASW does not follow this rule. We need to make sure 
emails never get sent to a customer / sales rep from a non-production system. We use [mailtrap.io](mailtrap.io) or a local 
 [mailcatcher server](mailcatcher.me) to intercept emails and deliver them to a privately-shared inbox for testing.

## File Uploads
File uploads require long-running processes to transfer large payloads.  This type of connection and traffic differs 
significantly from normal web browsing activity. Opening the webserver up to long-running processes with large payloads 
would require opening up firewall and common default connection configurations that open the webserver to attack. 
** Do not handle file uploads through the webserver. ** Instead use a front-end client library that connects the end
user to a file upload service that can make files available through a common integration (e.g. AWS S3).  The current approved
file upload service is [https://www.bytescale.com/](https://www.bytescale.com/).  Do not implement a server-to-server
implementation where the client first uploads to the application and then the application stores files in Bytescale.  This
would still require opening connections and firewall rules to the application's webserver.

## Logging
Ensure your application meets the PSR-3 (RFC 2119) requirement for logging by using 
 [Monolog](https://seldaek.github.io/monolog/) and log all important events throughout development including but not
 limited to:

  - Unauthorized access attempts
  - Successful user authentication
  - Failure to connect to remote systems (web services, database server)
  - Failure to perform critical business transactions
  - Key transactions between 3rd party systems (like payment authorization)

## Application Package/Library Management
[Composer](https://getcomposer.org/) will be used to manage dependencies and their versions as part of PSR-4.
  - Be sure to include any required core PHP extensions https://getcomposer.org/doc/01-basic-usage.md#platform-packages
  as part of the *inventory for your server requirements*
  - For security, any 3rd party packages must be approved by an architect or devsecops engineer.
  - Note: We do not host Wordpress sites on our infrastructure for security reasons. We currently use WPEngine for 
  managing a secure-as-possible Wordpress hosting environment.

## Configuration Management and Secrets
Require `vlucas/phpdotenv` as a composer dependency and maintain environment-specific configurations and secrets in 
 your .env file. Be sure to never commit your .env configuration to git and always maintain a `.env.example` file with 
 only keys for which configurations are required to run the application.

```
# .gitignore
*.env
!.env.example
```

## Testing
A minimal amount of tests should be created around the critical parts of the system and integration points using 
[Codeception](http://codeception.com/install) or PHPUnit closer to the end or after the project's launch.

Software isn't the product to our customers - it's a service we provide. We will take shortcuts when writing tests because
they take more time than they add value when our codebases change and require rewriting tests in order to make them pass.
Even then, they still only test what we know or expect to be bugs. "Testing shows the presence, not the absence, of bugs." 
-Dijkstra [IV. Structured Programming, "Clean Architecture" by Robert C. Martin]

In other words, our applications can be proven to be broken by tests, but cannot be proven bug-free and operational through 
tests. The value of our agility and velocity to provide service to our customers is higher than the risk of the potential to 
catch a critical bug *as long as our agility and velocity to respond to infrequent critical bugs remains in good standing.*


## Comments and Naming
 Help future you and other developers who will be inheriting your code know the reason for the business logic. This is 
 the WHY and not the WHAT; the WHAT should be evident in the code itself (if it's not, consider refactoring).


The bad:
```
  // update the order with added products
  public function update($input) {
      // loop through products
      foreach($input as $product) { 
          ...
```


The good:
```
  /**
   * Add a Product Collection (eg. Wishlist, Saved Cart, ReOrder) to the order
   */
  public function addProductsFromCollection(ProductCollectionInterface $collection) {
      foreach($collection as $product) {
          ...
```


## Application Development

### Coding Style & Formatting
  - PHP FIG PSR-2 can be achieved in a number of ways
    - Set your IDE's Code Style Scheme (eg. PHPStorm) to PSR-1/PSR-2
    - Install a commandline linter, like codesniffer `phpcs --standard=PSR2`
  - Develop your application, it's components, and modules in its own namespace (eg. Geigerdotcom), 
  organized by resources (eg. PriorityPlus\Order\LineItem\Event\ChangeLineItem, PriorityPlus\Order\Export\Asw\PPlusToAswExportCommand)
  and **not** by design patterns (eg. PriorityPlus\Events\PlaceOrder, PriorityPlus\Command\PlaceOrder, PriorityPlus\Broadcast\PlaceOrder)
  - Hierarchy suggests dependency while adjacency suggests loosely coupled components that do not depend on each other.

### Standard Naming Conventions
Through data normalization, entities tend to have a common set of unique attributes. Only when an entity is de-normalized 
should we prefix an attribute name with its other entity name.  For example, don't repeat "item" in \`item\`.\`item_name\`,
but do prefix a de-normalized entity for \'item\' when it's included entity contains another entity, like in \`item\`.\`vendor_name\`.

  - `id` is a **single field primary key** identity owned and **controlled by the database**. In some cases, 
    a uuid could be used as a primary key instead.
    
  - `code` is a character identity referencing a 3rd party system or a user-supplied number or code that they consider
    to be unique.
    
  - While the Item# (for example) may be unique to the business user, they may be working with the context of their
    business unit and not of an enterprise system that works with other business units and thousands of other vendors.
    A unique code to a user (eg. Item#) may require an additional field to truly make the field unique 
    (eg. BusinessUnit). An id will ensure uniqueness, while allowing the business users to choose or even change their 
    codes without affecting the system.  If a business wanted to change their codes (and they were used as the primary 
    key), the database would have to not only update and reindex all of its entity records, it would have to update 
    all foreign keys (possibly millions of records). If a foreign key wasn't defined on a table, all of that data would be
    broken and inaccessible, or worse: referencing the wrong data. And that is why we have a programmatic system id 
    separate from what business users expect to use and see as a unique code.
    
  - We choose the term "_code_" over "_number_", because of semantics; "A374-DC0" and "WC8103005BLK" are not numeric.
    "Product Number" in this case would be a user-defined label of `item.code` 
    
  - `{verbs}_{noun}` booleans (eg. `is_active`, `requires_approval`, `has_cooties`, `eats_glue`). 
    Follow the verbs_noun convention because there is a rare possibility (one I've never encountered) where a field 
    could be misunderstood to be a tiny integer and not a boolean (eg. min_qty as a tinyint could be understood to 
    either have a really limited minimum quantity or is only used to indicate whether minimum quantities apply as a 
    has_min_qty). Boolean verbs_noun convention eliminates the possibility of ambiguity between the two types. Also, 
    everyone is familiar and likes this pattern anyway.

### Types & Encoding
  - Be conscious of character encoding. UTF-8 everything if you can. But if you're working with ASW, you must account 
  for DB2's ASCII character encoding. Transmitting/Interpretting a wrong encoding will produce å¤?�¥ results. 
  [Stringy](https://github.com/danielstjules/Stringy#toascii-string-language--en--bool-removeunsupported--true-) can 
  help with converting utf-8 characters to ASCII.
  - Performing calculations on floating point numbers will produce rounding errors. For example,
    `"floor((0.1+0.7)*10) will usually return 7 instead of the expected 8, since the internal representation will be 
    something like 7.9999999999999991118"`.
  Applications performing calculations on floating point decimals must do so by one or more of the following ways:
    - Using [moneyphp/money](https://packagist.org/packages/moneyphp/money) with ext-bcmath and ext-intl extensions
    - Using [BCMath](http://php.net/manual/en/book.bc.php)
    - Converting the floating point to a whole integer, followed by performing the calculation, and then converting the 
      integer back to its original decimal (floating point) form.
    - Using [GMP](http://php.net/manual/en/book.gmp.php) *(currently unsupported)* 

### Security
  - Applications must meet PCI-DSS Compliance. Criteria can be referenced in other documents maintained by the 
  DevSecOps team.
  - Passwords, keys, tokens, and other configurable secret information must not be stored anywhere at any time in git
   history.
  - The application must not be vulnerable to 
  [OWASP's Top 10](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project#tab=Main) attack vectors. 
  Use their [cheat sheet](https://www.owasp.org/index.php/OWASP_Cheat_Sheet_Series#tab=Master_Cheat_Sheet) as a guide
  during development. Some of these areas are repeated and more finely scoped in this section.
  - Your application should use PDO with proper parameter binding or a library abstraction using PDO to 
    prevent common security issues with [SQL injection](https://www.owasp.org/index.php/SQL_Injection).
  - User input must be filtered, encoded, and/or blocked to prevent
  [XSS](https://www.owasp.org/index.php/Cross-site_Scripting_%28XSS%29).
  - [Secure cookies](https://www.owasp.org/index.php/SecureFlag) must be enabled and a strong encryption should be 
  used on the data it stores.
  - Forms must include and validate against a server-side generated token to prevent 
  [Cross-Site Request Forgery](https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)).
  - External sources referenced on an html page should use 
    [Subresource Integrity](https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity) 
    checks to prevent malicious code from executing on our applications from a compromised remote host.
  - Third party javascript components must be approved before use (eg. live chat, sentry.io).
  - A production system should:
    - **Never** record or log password, credit card, or any sensitive information.
    - **Never** have debugging enabled (not to be confused with logging).
    - **Never** expose sensitive information including but not limited to phpinfo(), environment variables, or emit 
      stack trace information as part of an error message returned to the user.
    - **Always** employ a secure communication publicly over HTTPS.
   - Explicit owner/group/user file permissions must be set for every file and directory in the application. Projects
    must use the front-controller pattern `/public/index.php` to manage the 
    scope of what's publicly accessible - so that the source, logs, git history, etc. is not accidentally 
    accessible.

### Microsoft Azure AD Authentication 

Read [azure-active-directory.md](azure-active-directory.md).

### Analytics
  - All applications require a Google Analytics tracking code to help understand application usage (including 
  our customers, partners, and internal associates).
  Please ask IT for a [Google Analytics](https://analytics.google.com) tracking code and administrative access to
  the property (if needed).
  
### Monitoring 
A `/check-status` endpoint should be created for all applications, providing some or all dependency checks to determine
whether the application is online, offline, or degraded in some way. 
  
**Example:** the application will `try` to connect to the AS400. When the connection fails, the application will
 `catch` the error and emit an `HTTP 503` response to the monitoring client. The document type can be `application/json`
 or `text/html`, but should provide a human-readable way to identify what is causing the system to fail.

## Frontend Development
Use [Twitter's Bootstrap](http://getbootstrap.com) framework for responsive design.

In general, stay away from single-page application frameworks and package managers - at least for now. We've 
 used Browserify, Gulp, and Webpack as well as Angular, Vue, and Ember. In five years time, we don't want to maintain, 
 support, or have to replace 12 different and outdated javascript frameworks. Browser versions, compatibility, 
 security, and the fragile nature of javascript errors make single-page application frameworks a high risk.
